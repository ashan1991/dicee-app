//
//  ViewController.swift
//  Dicee
//
//  Created by Ashan Lakshith on 7/7/18.
//  Copyright © 2018 Ashan Lakshith. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var randomDiceVeriable01 : Int = 0;
    var randomDiceVeriable02 : Int = 0;
    let diceeArray = ["dice1","dice2","dice3","dice4","dice5","dice6"]

    @IBOutlet weak var diceImageView01: UIImageView!
    @IBOutlet weak var diceImageView02: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateRandomDicee()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func rollButtonPress(_ sender: UIButton) {
        updateRandomDicee()
    }
    
    func updateRandomDicee(){
        randomDiceVeriable01 = Int(arc4random_uniform(6))
        randomDiceVeriable02 = Int(arc4random_uniform(6))
        
        diceImageView01.image  = UIImage(named: diceeArray[randomDiceVeriable01])
        diceImageView02.image  = UIImage(named: diceeArray[randomDiceVeriable02])
        //print(randomDiceVeriable01)
        
    }
    override func motionEnded(_ motion: UIEventSubtype, with event: UIEvent?) {
        updateRandomDicee()
    }
    
}

